#include<iostream>
#include<sys/time.h>
#include<fstream>

#define K 306
#define W 50

using namespace std;

class Graf;
class Wierzcholek;
class Krawedz;

/******KLASA KRAWEDZ******/
class Krawedz{

public:

  int waga;
  Wierzcholek* v1;
  Wierzcholek* v2;

};
/******KLASA WIERZCHOLEK******/
class Wierzcholek{

public:

  int wartosc;
  int stopien;
  Krawedz** polaczenia = new Krawedz*[500000];
  bool odw=0;
};

/******KLASA GRAF******/
class Graf: public Wierzcholek, public Krawedz{

public:
  int lkraw=K;
  int lwierz=W;
  Wierzcholek* listaWierzcholkow = new Wierzcholek[lwierz];
  Krawedz* listaKrawedzi = new Krawedz[lkraw];
  int* listaSasiedztwa[W];
};

void tworzenie(int lwierz, int lkraw, int** tab, Graf &G){

  int numerKrawedzi=0;
  int x, y;
  for(int i=0; i<lkraw; i++){
    x=tab[0][i];
    y=tab[1][i];
    G.listaKrawedzi[numerKrawedzi].v1=&G.listaWierzcholkow[x];
    G.listaKrawedzi[numerKrawedzi].v2=&G.listaWierzcholkow[y];
    G.listaKrawedzi[numerKrawedzi].waga=tab[2][i];
    numerKrawedzi++;
  }

  for(int i=0; i<lwierz; i++)
    G.listaWierzcholkow[i].wartosc=i;

  int a, b;
  for(int i=0; i<numerKrawedzi; i++){
    x=G.listaKrawedzi[i].v1->stopien;
    y=G.listaKrawedzi[i].v2->stopien;
    a=G.listaKrawedzi[i].v1->wartosc;
    b=G.listaKrawedzi[i].v2->wartosc;
    G.listaWierzcholkow[a].stopien++;
    G.listaWierzcholkow[a].polaczenia[x]=&G.listaKrawedzi[i];
    G.listaWierzcholkow[b].stopien++;
    G.listaWierzcholkow[b].polaczenia[y]=&G.listaKrawedzi[i];
  }

  for(int i=0; i<lwierz; i++)
    for(int j=0; j<G.listaWierzcholkow[i].stopien; j++){
      cout<<G.listaWierzcholkow[i].wartosc<<" przez "
	  <<G.listaWierzcholkow[i].polaczenia[j]->waga<<" do ";
      if(G.listaWierzcholkow[i].wartosc==G.listaWierzcholkow[i].polaczenia[j]->v1->wartosc)
	cout<<G.listaWierzcholkow[i].polaczenia[j]->v2->wartosc<<endl;
      else
	cout<<G.listaWierzcholkow[i].polaczenia[j]->v1->wartosc<<endl;
    }

  for(int i=0; i<lwierz; i++){
    G.listaSasiedztwa[i]=new int[G.listaWierzcholkow[i].stopien];
    for(int j=0; j<G.listaWierzcholkow[i].stopien; j++)
      G.listaSasiedztwa[i][j]=0;
  }

  for(int i=0; i<lwierz; i++)
    for(int j=0; j<G.listaWierzcholkow[i].stopien; j++)
      if(G.listaWierzcholkow[i].wartosc==G.listaWierzcholkow[i].polaczenia[j]->v1->wartosc)
	G.listaSasiedztwa[i][j]=G.listaWierzcholkow[i].polaczenia[j]->v2->wartosc;
      else
	G.listaSasiedztwa[i][j]=G.listaWierzcholkow[i].polaczenia[j]->v1->wartosc;

  cout<<endl;
  for(int i=0; i<lwierz; i++){
    cout<<i<<"\t";
    for(int j=0; j<G.listaWierzcholkow[i].stopien; j++)
      cout<<G.listaSasiedztwa[i][j]<<"   ";
    cout<<endl;
  }

  return;
}
/**FUNKCJA ZAMIENIAJACA MIEJSCAMI WARTOSCI**/
void zamien(Krawedz &a, Krawedz &b){
  Krawedz tmp;
  tmp.waga=a.waga;
  tmp.v1=a.v1;
  tmp.v2=a.v2;

  a.waga=b.waga;
  a.v1=b.v1;
  a.v2=b.v2;

  b.waga=tmp.waga;
  b.v1=tmp.v1;
  b.v2=tmp.v2;

  return;
}
/**FUNKCJA DZIELACA TABLICE**/
int Podziel(Krawedz *Krawedzie, int l, int r){ //l - lewa strona, p - prawa strona
  int indeks; //indeks elementu nie bioracego udzialu w sortowaniu (pivot)
  int wartosc;//wartosc pivota
  int pozycja;//aktualna pozycja

  indeks=l+(r-l)/2; //wybranie pivota z polowy tablicy
  wartosc=Krawedzie[indeks].waga;
  zamien(Krawedzie[indeks], Krawedzie[r]); //przeniesienie pivota na koniec

  pozycja=l;
  for(int i=l; i<=r-1; i++)
    {
      if(Krawedzie[i].waga<wartosc) //jesli element jest mniejszy
	{                    //zostaje przeniesiony na lewo
	  zamien(Krawedzie[i], Krawedzie[pozycja]);
	  pozycja=pozycja+1;
	}
    }
  zamien(Krawedzie[pozycja], Krawedzie[r]); //przywrocenie pivota w odpowiednie
  return pozycja;                       //miejsce
}
/****QUICKSORT****/
void quicksort(Krawedz *Krawedzie, int l, int r){
  int i;
  if(l<r)
    {
      i=Podziel(Krawedzie, l, r);  //podzial tablicy na 2 czesci
      quicksort(Krawedzie, l, i-1);//sortowanie lewej strony
      quicksort(Krawedzie, i+1, r);//sortowanie prawej strony
    }
}

/**********************************************/
/*******************PRIM***********************/
/**********************************************/
Graf Prim(Graf G){

  Graf Drzewo;
  Krawedz* Krawedzie=new Krawedz[K];
  int suma=0;
  int liczbaKrawedzi=0;
  Wierzcholek w;

  int j=0;
  int i=0;
  w=G.listaWierzcholkow[0];
  for(int l=0; l<(W-1); l++){
  for(int i=0; i<w.stopien; i++)
    Krawedzie[liczbaKrawedzi+i]=*w.polaczenia[i];
  liczbaKrawedzi+=w.stopien;
  quicksort(Krawedzie, 0, liczbaKrawedzi-1);

  for(i=0 ;i<liczbaKrawedzi; i++)
    if(Krawedzie[i].v1->odw==0 &&  Krawedzie[i].v2->odw==0){
      Drzewo.listaKrawedzi[j].waga=Krawedzie[i].waga;
      Drzewo.listaKrawedzi[j].v1=Krawedzie[i].v1;
      Drzewo.listaKrawedzi[j].v2=Krawedzie[i].v2;
      j++;
      if(w.wartosc==Krawedzie[i].v1->wartosc){
	Krawedzie[i].v1->odw=1;
	w=*Krawedzie[i].v2;
      }
      else{
	Krawedzie[i].v2->odw=1;
	w=*Krawedzie[i].v1;
      }
      break;
    }
  }

  cout<<endl;
  for(int i=0; i<j; i++){
    cout<<Drzewo.listaKrawedzi[i].v1->wartosc<<"-"<<Drzewo.listaKrawedzi[i].v2->wartosc;
    cout<<" waga = "<<Drzewo.listaKrawedzi[i].waga<<endl;
    suma+=Drzewo.listaKrawedzi[i].waga;
  }
  cout<<"Suma wag = "<<suma<<endl;
  return Drzewo;
}


int main(){

  ifstream Plik("graf.txt");
  Graf G, Drzewo;
  int lkrawedzi=K;
  int lwierzcholkow=W;
  int** tab=new int*[3];
  int nowaDana;
  timeval t1, t2;
  double elapsedTime;

  for(int i=0; i<3; i++)
    tab[i]=new int[lkrawedzi];

  G.lkraw=lkrawedzi;

  for(int i=0; i<3; i++)
    for(int j=0; j<lkrawedzi; j++){
      Plik>>nowaDana;
      tab[i][j]=nowaDana;
    }

  cout<<endl;

  tworzenie(lwierzcholkow, lkrawedzi, tab, G);

  gettimeofday(&t1, NULL);

  Drzewo=Prim(G);

  gettimeofday(&t2, NULL);
  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
  cout<<"Znaleziono w czasie :"<<elapsedTime<<"ms."<<endl;

  return 0;
}
