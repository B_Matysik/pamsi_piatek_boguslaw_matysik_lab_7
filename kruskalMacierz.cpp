#include<iostream>
#include<sys/time.h>
#include<fstream>

#define K 306
#define W 50
using namespace std;

class Graf;
class Wierzcholek;
class Krawedz;

/******KLASA KRAWEDZ******/
class Krawedz{

public:

  int waga;
  Wierzcholek* v1;
  Wierzcholek* v2;

};
/******KLASA WIERZCHOLEK******/
class Wierzcholek{

public:

  int wartosc;
  int stopien;
  Krawedz** polaczenia = new Krawedz*[5000000];
  bool odw=0;
};

/******KLASA GRAF******/
class Graf: public Wierzcholek, public Krawedz{

public:
  int lkraw=K;
  int lwierz=W;
  Wierzcholek* listaWierzcholkow = new Wierzcholek[W];
  Krawedz* listaKrawedzi = new Krawedz[K];
  int macierzSasiedztwa[W][W];
};
void tworzenie(int lwierz, int lkraw, Graf G){

  int numerKrawedzi=0;

  cout<<endl;

  for(int i=0; i<lwierz; i++)
    for(int j=0; j<i; j++)
      if(G.macierzSasiedztwa[i][j]!=0)
	{
	  G.listaKrawedzi[numerKrawedzi].v1=&G.listaWierzcholkow[i];
	  G.listaKrawedzi[numerKrawedzi].v2=&G.listaWierzcholkow[j];
	  G.listaKrawedzi[numerKrawedzi].waga=G.macierzSasiedztwa[i][j];
	  numerKrawedzi++;
	}

  for(int i=0; i<lwierz; i++)
    G.listaWierzcholkow[i].wartosc=i;

  int x, y;
  for(int i=0; i<numerKrawedzi; i++){
    x=G.listaKrawedzi[i].v1->stopien;
    y=G.listaKrawedzi[i].v2->stopien;
    G.listaKrawedzi[i].v1->polaczenia[x]=&G.listaKrawedzi[i];
    G.listaKrawedzi[i].v1->stopien++;
    G.listaKrawedzi[i].v2->polaczenia[y]=&G.listaKrawedzi[i];
    G.listaKrawedzi[i].v2->stopien++;
  }


  return;
}
/**FUNKCJA ZAMIENIAJACA MIEJSCAMI WARTOSCI**/
void zamien(Krawedz &a, Krawedz &b){
  Krawedz tmp;
  tmp.waga=a.waga;
  tmp.v1=a.v1;
  tmp.v2=a.v2;

  a.waga=b.waga;
  a.v1=b.v1;
  a.v2=b.v2;

  b.waga=tmp.waga;
  b.v1=tmp.v1;
  b.v2=tmp.v2;

  return;
}
/**FUNKCJA DZIELACA TABLICE**/
int Podziel(Krawedz *Krawedzie, int l, int r){ //l - lewa strona, p - prawa strona
  int indeks; //indeks elementu nie bioracego udzialu w sortowaniu (pivot)
  int wartosc;//wartosc pivota
  int pozycja;//aktualna pozycja

  indeks=l+(r-l)/2; //wybranie pivota z polowy tablicy
  wartosc=Krawedzie[indeks].waga;
  zamien(Krawedzie[indeks], Krawedzie[r]); //przeniesienie pivota na koniec

  pozycja=l;
  for(int i=l; i<=r-1; i++)
    {
      if(Krawedzie[i].waga<wartosc) //jesli element jest mniejszy
	{                    //zostaje przeniesiony na lewo
	  zamien(Krawedzie[i], Krawedzie[pozycja]);
	  pozycja=pozycja+1;
	}
    }
  zamien(Krawedzie[pozycja], Krawedzie[r]); //przywrocenie pivota w odpowiednie
  return pozycja;                       //miejsce
}
/****QUICKSORT****/
void quicksort(Krawedz *Krawedzie, int l, int r){
  int i;
  if(l<r)
    {
      i=Podziel(Krawedzie, l, r);  //podzial tablicy na 2 czesci
      quicksort(Krawedzie, l, i-1);//sortowanie lewej strony
      quicksort(Krawedzie, i+1, r);//sortowanie prawej strony
    }
}

/**********************************************/
/*******************KRUSKAL********************/
/**********************************************/
Graf Kruskal(Graf G){

  Graf Drzewo;
  Krawedz* Krawedzie=G.listaKrawedzi;
  int suma=0;

  quicksort(Krawedzie, 0, K-500);
  cout<<endl;
  for(int i=0; i<K; i++){
    cout<<Krawedzie[i].v1->wartosc<<"-"<<Krawedzie[i].v2->wartosc;
    cout<<" waga = "<<Krawedzie[i].waga<<endl;
  }

  int j=0;
  for(int i=0 ;i<K; i++)
    if(Krawedzie[i].v1->odw==0 ||  Krawedzie[i].v2->odw==0){
      Drzewo.listaKrawedzi[j].waga=Krawedzie[i].waga;
      Drzewo.listaKrawedzi[j].v1=Krawedzie[i].v1;
      Drzewo.listaKrawedzi[j].v2=Krawedzie[i].v2;
      Krawedzie[i].v1->odw=1;
      Krawedzie[i].v2->odw=1;
      j++;
    }

  cout<<endl;
  for(int i=0; i<j; i++){
    cout<<Drzewo.listaKrawedzi[i].v1->wartosc<<"-"<<Drzewo.listaKrawedzi[i].v2->wartosc;
    cout<<" waga = "<<Drzewo.listaKrawedzi[i].waga<<endl;
    suma+=Drzewo.listaKrawedzi[i].waga;
  }
  cout<<"Suma wag = "<<suma<<endl;
  return Drzewo;
}

int main(){

  ifstream Plik("graf.txt");
  Graf G, Drzewo;
  int lkrawedzi=K;
  int lwierzcholkow=W;
  int** tab=new int*[3];
  int nowaDana;
  timeval t1, t2;
  double elapsedTime;

  for(int i=0; i<3; i++)
    tab[i]=new int[lkrawedzi];

  G.lkraw=lkrawedzi;

  for(int i=0; i<lwierzcholkow; i++)
    for(int j=0; j<lwierzcholkow; j++)
      G.macierzSasiedztwa[i][j]=0;

  for(int i=0; i<3; i++)
    for(int j=0; j<lkrawedzi; j++){
      Plik>>nowaDana;
      tab[i][j]=nowaDana;
    }

  cout<<endl;
  int x, y;
  for(int i=0; i<lkrawedzi; i++){
    x=tab[0][i];
    y=tab[1][i];
    if(x==y)
      G.macierzSasiedztwa[x][y]=0;
    else
      if(x>y)
	G.macierzSasiedztwa[x][y]=tab[2][i];
      else
	G.macierzSasiedztwa[y][x]=tab[2][i];
  }

  tworzenie(lwierzcholkow, lkrawedzi, G);

  gettimeofday(&t1, NULL);

  Drzewo=Kruskal(G);

  gettimeofday(&t2, NULL);
  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
  cout<<"Znaleziono w czasie :"<<elapsedTime<<"ms."<<endl;

  return 0;
}
